#!/bin/bash
for arg;do
file=$(echo "$arg" | sed 's/\.\w*$/''/')
if [ ! -f "$arg" ]; then
  echo "ERROR: '$arg' not found."
else
  width=$(mediainfo --Inform="Video;%Width%" $arg)
  height=$(mediainfo --Inform="Video;%Height%" $arg)
  bitrate=$(mediainfo --Inform="Video;%BitRate%" $arg)
  format=$(mediainfo --Inform="Video;%Format%" $arg)
  if [ "$format" != "HEVC" ]; then
    if [ $height -gt 1080 ] && [ $width -gt 1920 ] && [ $bitrate -gt 9047441 ]; then
      param="Roku 2160p60 4K HEVC Surround"
    elif [ $height -le 1080 ] && [ $height -gt 720 ]; then
      param="H.265 MKV 1080p30"
    elif [ $height -le 720 ] && [ $height -gt 576 ]; then
      param="H.265 MKV 720p30"
    elif  [ $height -le 576 ] && [ $height -gt 480 ]; then
      param="H.265 MKV 576p25"
    elif  [ $height -le 480 ]; then
      param="H.265 MKV 480p30"
    else
      param="Unknown not 4K"
    fi
  else
#    if [ $height -gt 1080 ] && [ $width -gt 1920 ] && [ $bitrate -gt 9047441 ]; then
    if [ $height -gt 1080 ] && [ $width -gt 1920 ] && [ $bitrate -gt 11173047 ]; then
      param="Roku 2160p60 4K HEVC Surround"
    else
      param="Unknown 4K"
    fi
  fi
  if echo $param | grep Unknown; then
    echo $arg height $height width $width param $param bitrate $bitrate
  else
    HandBrakeCLI -Z "$param" -i $arg -o ${arg%.*}.new.mkv
  fi
fi
done
